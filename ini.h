#pragma once
#include <unordered_map>
#include <string>

namespace snippet {

	namespace config {
		using namespace std;
		class ini : private unordered_map<string, string> {
		private:
			ini() { ; }
			ini(unordered_map<string, string>& options) { swap(options); }
		public:

			inline unordered_map<string, string> values() {
				return *this;
			}

			/*
			* Check options exist
			* @return bool
			*/
			inline bool is(string name) const {
				return find(name) != end();
			}

			/*
			* Get parsed command line option value
			* @return const char*
			*/
			inline const string& get(string name, string default_value) const {
				auto it = find(name);
				if (it != end()) {
					return it->second;
				}
				return default_value;
			}

			static ini read(const char* filepathname) throw string {
				static const regex re(R"((?:^\[\s*([\w-\.:]+)\s*\])|(?:^\s*(.*?)\s*=\s*(?:(?:('|")(.+)(?:\3))|(?:([^#]+)))))");
				FILE* hfile = fopen(filepathname,"rt");
				if (hfile != nullptr) {
					char buffer[1024];
					string section;
					unordered_map<string, string> options;
					while (!feof(hfile) && fgets(buffer, 1024, hfile)) {
						string in(buffer);
						if (regex_search(in, match, re, std::regex_constants::match_not_null) && match.size() > 1) {
							if (!match.str(1).empty()) {
								section = match.str(1);
								transform(section.begin(), section.end(), section.begin(), ::tolower);
							}
							else if (!match.str(2).empty()) {
								string name(match.str(2)), value(match.str(4) + match.str(5));
								transform(name.begin(), name.end(), name.begin(), ::tolower);
								str::trim(value);
								if (!section.empty()) {
									string id = section + "." + name;
									options.emplace(id, value);
								}
								else {
									options.emplace(name, value);
								}
							}
						}
					}
					fclose(hfile);
				}
				else {
					throw strerror(errno);
				}
				return options;
			}
		};
	}
}