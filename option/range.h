#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include "./helpers/convert.h"
#include "./helpers/utils.h"

namespace snippet {
	namespace config {
		namespace option {
			using namespace std;

			template<class TP = int, typename CVT = cvt::to_int<TP>, char SET_SEP = ',', char RANGE_SEP = '-'>
			class range : vector<TP> {
			public:
				typedef typename vector<TP>::iterator		iterator;
			public:
				inline void operator = (string&& val) {
					if (!val.empty()) {
						CVT cvt;
						vector<TP>::clear();
						string value = val;

						config::utils::trim(value);
						string sep,rsep; 
						sep = SET_SEP; 
						rsep = RANGE_SEP;

						auto&& set_list = config::utils::explode(move(sep), move(value));
						for (auto&& s : set_list) {
							config::utils::trim(s);
							auto&& range_list = config::utils::explode(move(rsep), move(s) );
							if (range_list.size() == 1) {
								TP i = cvt(s);
								vector<TP>::emplace_back(i);
							}
							else if (range_list.size() > 1) {
								auto&& fv = range_list.front(), &&lv = range_list.back();
								TP f = cvt(fv);
								TP t = cvt(lv.empty() ? fv : lv);
								for (TP i = f; i <= t; i++) {
									vector<TP>::emplace_back(i);
								}
							}
						}
					}
				}
				inline size_t size() { return vector<TP>::size(); }
				TP operator[](size_t indx) { return vector<TP>::at(indx); }
				inline iterator begin() { return vector<TP>::begin(); }
				inline iterator end() { return vector<TP>::end(); }
			};
		}
	}
}