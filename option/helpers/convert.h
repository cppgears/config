#pragma once
#include <string>

namespace snippet {
	namespace config {
		namespace cvt {
			using namespace std;
			class to_string {
			public:
				inline string operator ()(const string& v) const { return v; }
			};
			template<typename NTYPE = int>
			class to_int {
				inline unsigned long long str_to_ull(const string&& val) const { return stoull(val); }
			public:
				inline NTYPE operator ()(const string& v) const { return (NTYPE)str_to_ull(move(v)); }
			};
		}
	}
}


