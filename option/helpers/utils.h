#pragma once
#include <list>
#include <string>

namespace snippet {
	namespace config {
		namespace utils {
			using namespace std;
			static inline void trim(string& value) {
				value.erase(value.begin(), find_if(value.begin(), value.end(), [](int ch) {
					return !isspace(ch);
				}));

				value.erase(find_if(value.rbegin(), value.rend(), [](int ch) {
					return !isspace(ch);
				}).base(), value.end());
			}
			
			static inline list<string> explode(const std::string&& sep, string&& value) {
				list<string> list;
				size_t start = 0;
				size_t end = value.find(sep);
				string piece;
				for (; end != string::npos; start = end + 1, end = value.find(sep, start))
				{
					piece.assign(value.begin() + start, value.begin() + end);
					trim(piece);
					if (piece.empty()) {
						break;
					}
					list.emplace_back(piece);
				}
				piece.assign(value.begin() + start, value.end());
				trim(piece);
				if (!piece.empty()) {
					list.emplace_back(piece);
				}
				return list;
			}
		}
	}
}