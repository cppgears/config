#pragma once
#include <string>
#include <cstring>
#include <cctype>

namespace snippet {
	namespace config {
		namespace option {
			template<typename T>
			class value {
				T	_value;
			public:
				value(T&& deflt = T()) : _value(deflt) { ; }
				inline const T& operator ()() { return _value; }
				inline void operator = (std::string&& val) { _value = T(std::move(val)); }
				inline void operator = (T&& val) { _value = val; }

				static inline unsigned long long str_to_ull(std::string&& val) { return std::stoull(val); }
			};

			template<>
			inline void value<int16_t>::operator=(std::string&& val) { _value = (int16_t)str_to_ull(std::move(val)); }
			template<>
			inline void value<uint16_t>::operator=(std::string&& val) { _value = (uint16_t)str_to_ull(std::move(val)); }

			template<>
			inline void value<int32_t>::operator=(std::string&& val) { _value = (int32_t)str_to_ull(std::move(val)); }
			template<>
			inline void value<uint32_t>::operator=(std::string&& val) { _value = (uint32_t)str_to_ull(std::move(val)); }

			template<>
			inline void value<int64_t>::operator=(std::string&& val) { _value = (int64_t)str_to_ull(std::move(val)); }
			template<>
			inline void value<uint64_t>::operator=(std::string&& val) { _value = (uint64_t)str_to_ull(std::move(val)); }

			template<>
			inline void value<float>::operator=(std::string&& val) { _value = std::stof(val); }
			template<>
			inline void value<double>::operator=(std::string&& val) { _value = std::stod(val); }
			template<>
			inline void value<long double>::operator=(std::string&& val) { _value = std::stold(val); }

			template<>
			inline void value<bool>::operator=(std::string&& val) { _value = !val.empty() && (val[0] == '1' || strncasecmp(val.c_str(), "on", 2) == 0 || strncasecmp(val.c_str(), "true", 4) == 0); }

		}
	}
}