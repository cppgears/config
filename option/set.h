#pragma once
#include <string>
#include <unordered_set>
#include <list>
#include <algorithm>
#include "./helpers/convert.h"
#include "./helpers/utils.h"

namespace snippet {
	namespace config {
		namespace option {
			using namespace std;
			template<class TP = string, typename CVT = cvt::to_string, char SEP = ','>
			class set : unordered_set<TP> {
			public:
				typedef typename unordered_set<TP>::iterator		iterator;
			public:
				inline void operator = (string&& val) {
					unordered_set<TP>::clear();
					if (!val.empty()) {
						CVT cvt;
						string sep; sep = SEP;
						for (auto&& part : config::utils::explode(move(sep),move(val))) {
							this->emplace(cvt(part));
						}
					}
				}
				inline bool isset(TP&& val) {
					return find(val) != unordered_set<TP>::end();
				}
				inline iterator begin() { return unordered_set<TP>::begin(); }
				inline iterator end() { return unordered_set<TP>::end(); }
			};
		}
	}
}