#pragma once
#include <unistd.h>
#include <string>
#include <algorithm>
#include <regex>
#include <unordered_map>

namespace snippet {
	namespace config {
		namespace option {

			using namespace std;

			class dsn {
				string	_string;
				string	_proto;
				string	_path;
				unordered_map<string, string>	_params;

				inline void get_params(const string&& str) {
					regex re("&?(.*?)=([^&]*)", regex_constants::icase);
					sregex_iterator next(str.begin(), str.end(), re);
					sregex_iterator end;
					_params.clear();
					while (next != end) {
						smatch match = *next;
						_params.emplace(match[1].str(), match[2].str());
						next++;
					}
				}

			public:
				inline const string& param(const string&& name, const string&& default_value = {}) const {
					auto&& it = _params.find(name);
					if (it != _params.end()) {
						return it->second;
					}
					return default_value;
				}

				inline void operator = (string&& val) {
					static const regex re(R"(^(?:(.*?)://)?(.*?)(?:\?(.*?))?$)");
					_string = val;
					smatch match;
					if (regex_search(_string, match, re) && match.size() > 1) {
						_proto = match.str(1); transform(_proto.begin(), _proto.end(), _proto.begin(), ::tolower);
						if (_proto.empty()) _proto = "file";
						_path = match.str(2);
						get_params(match.str(3));
					}
				}
				inline const string& proto() const { return _proto; }
				inline const string& url() const { return _string; }
				inline bool db(string& provider, string& host, string& username, string& pwd, string& schema, int& port) const {
					static const regex re(R"(^(?:([^:@]*)(?::([^@]*?))?@)?([^:/]+)(?::(\d+))?(?:/([^/?]+)/?)?\??(.*))");
					smatch match;
					provider = _proto;
					if (regex_search(_path, match, re) && match.size() > 1) {
						string _port(match.str(4));
						username = match.str(1);
						pwd = match.str(2);
						host = match.str(3);
						schema = match.str(5);
						!_port.empty() && (port = stoi(_port));
						return true;
					}
					return false;
				}
				inline string file() const {
					return _path;
				}
			};
		}
	}
}